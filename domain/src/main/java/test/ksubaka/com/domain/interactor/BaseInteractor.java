package test.ksubaka.com.domain.interactor;



import io.reactivex.Observable;
import test.ksubaka.com.data.repository.MoviesRepository;
import test.ksubaka.com.domain.interactor.thread.InteractorThread;

/**
 * Created by Lucian on 1/13/2017.
 */

public abstract class BaseInteractor<T> {
    private InteractorThread observeThread;
    private InteractorThread subscribeThread;
    protected MoviesRepository repository;

    BaseInteractor(InteractorThread observeThread, InteractorThread subscribeThread, MoviesRepository repository) {
        this.observeThread = observeThread;
        this.subscribeThread = subscribeThread;
        this.repository = repository;
    }

    public Observable<T> execute() {
        return operation()
                .subscribeOn(subscribeThread.scheduler())
                .observeOn(observeThread.scheduler());
    }

    protected abstract Observable<T> operation();
}
