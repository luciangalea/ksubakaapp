package test.ksubaka.com.domain.interactor.configuration;

/**
 * Created by Lucian on 1/18/2017.
 */

public interface Configuration {
    //return period to wait between characters typed by the user before triggering an API call
    long searchDebounce();

    //minimun length of the query the user has to enter to trigger an API call
    int minNumberOfCharactersForSearch();
}
