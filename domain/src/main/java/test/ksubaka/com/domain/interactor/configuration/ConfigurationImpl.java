package test.ksubaka.com.domain.interactor.configuration;

/**
 * Created by Lucian on 1/18/2017.
 */

public class ConfigurationImpl implements Configuration {
    @Override
    public long searchDebounce() {
        return 500;
    }

    @Override
    public int minNumberOfCharactersForSearch() {
        return 3;
    }
}
