package test.ksubaka.com.domain.interactor;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.repository.MoviesRepository;
import test.ksubaka.com.domain.interactor.thread.InteractorThread;

/**
 * Created by Lucian on 1/18/2017.
 */

public class GetMoviesInteractor extends BaseInteractor<List<Movie>> {
    private String query;

    @Inject
    GetMoviesInteractor(@Named("observeThread") InteractorThread observeThread,
                        @Named("subscribeThread") InteractorThread subscribeThread,
                        MoviesRepository repository) {
        super(observeThread, subscribeThread, repository);
    }

    public GetMoviesInteractor withData(String query) {
        this.query = query;
        return this;
    }

    @Override
    protected Observable<List<Movie>> operation() {
        return repository.getMovies(query);
    }
}
