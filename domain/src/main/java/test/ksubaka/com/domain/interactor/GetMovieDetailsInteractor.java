package test.ksubaka.com.domain.interactor;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.repository.MoviesRepository;
import test.ksubaka.com.domain.interactor.thread.InteractorThread;

/**
 * Created by Lucian on 1/18/2017.
 */

public class GetMovieDetailsInteractor extends BaseInteractor<MovieDetails> {
    private String id;

    @Inject
    GetMovieDetailsInteractor(@Named("observeThread") InteractorThread observeThread,
                              @Named("subscribeThread") InteractorThread subscribeThread,
                              MoviesRepository repository) {
        super(observeThread, subscribeThread, repository);
    }

    public GetMovieDetailsInteractor withData(String id) {
        this.id = id;
        return this;
    }

    @Override
    protected Observable<MovieDetails> operation() {
        return repository.getMovieDetails(id);
    }
}
