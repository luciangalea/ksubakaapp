package test.ksubaka.com.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import test.ksubaka.com.data.cache.Cache;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.entity.MoviesResult;
import test.ksubaka.com.data.repository.MoviesRepository;
import test.ksubaka.com.data.store.MoviesDataStore;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Lucian on 1/18/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class MoviesRepositoryTest {
    @Mock
    MoviesDataStore dataStore;
    @Mock
    Cache cache;

    private MoviesResult storeResult = new MoviesResult();
    private List<Movie> storeMovies = new ArrayList<>();
    private List<Movie> cacheMovies = new ArrayList<>();
    private MovieDetails storeMovieDetails = new MovieDetails();
    private MovieDetails cacheMovieDetails = new MovieDetails();

    private MoviesRepository repository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        storeResult.setMovies(storeMovies);

        repository = new MoviesRepository(cache, dataStore);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadMoviesFromCache() {
        String query = "test";
        cacheMovies.add(new Movie());

        when(dataStore.getMovies(query)).thenReturn(Observable.just(storeResult).delay(500, TimeUnit.MILLISECONDS));
        when(cache.getMovies(query)).thenReturn(Observable.just(cacheMovies));

        TestObserver<List<Movie>> observer = new TestObserver<>();
        repository.getMovies(query).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertComplete();
        observer.assertResult(cacheMovies);
        verify(cache, never()).storeMovies(any(List.class), any(String.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadMoviesFromStoreIfCacheEmpty() {
        String query = "test";

        when(dataStore.getMovies(query)).thenReturn(Observable.just(storeResult).delay(500, TimeUnit.MILLISECONDS));
        when(cache.getMovies(query)).thenReturn(Observable.just(cacheMovies));

        TestObserver<List<Movie>> observer = new TestObserver<>();
        repository.getMovies(query).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertComplete();
        observer.assertResult(storeMovies);
        verify(cache).storeMovies(storeMovies, query);
    }

    @Test
    public void testReadMovieDetailsFromCache() {
        String id = "test";

        when(dataStore.getMovieDetails(id)).thenReturn(Observable.just(storeMovieDetails).delay(500, TimeUnit.MILLISECONDS));
        when(cache.getMovieDetails(id)).thenReturn(Observable.just(cacheMovieDetails));

        TestObserver<MovieDetails> observer = new TestObserver<>();
        repository.getMovieDetails(id).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertComplete();
        observer.assertResult(cacheMovieDetails);
        verify(cache, never()).storeMovieDetails(any(String.class), any(MovieDetails.class));
    }

    @Test
    public void testReadMovieDetailsFromStoreIfCacheEmpty() {
        String id = "test";

        when(dataStore.getMovieDetails(id)).thenReturn(Observable.just(storeMovieDetails).delay(500, TimeUnit.MILLISECONDS));
        when(cache.getMovieDetails(id)).thenReturn(Observable.just(MovieDetails.dummy()));

        TestObserver<MovieDetails> observer = new TestObserver<>();
        repository.getMovieDetails(id).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertComplete();
        observer.assertResult(storeMovieDetails);
        verify(cache).storeMovieDetails(id, storeMovieDetails);
    }
}
