package test.ksubaka.com.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MoviesResult {
    @SerializedName("Search")
    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
