package test.ksubaka.com.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MovieDetails {
    private static MovieDetails DUMMY;

    @SerializedName("Poster")
    private String poster;
    @SerializedName("Title")
    private String title;
    @SerializedName("imdbID")
    private String imdbID;
    @SerializedName("Actors")
    private String actors;
    @SerializedName("Director")
    private String director;
    @SerializedName("Genre")
    private String genre;

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieDetails that = (MovieDetails) o;

        return imdbID.equals(that.imdbID);

    }

    @Override
    public int hashCode() {
        return imdbID.hashCode();
    }

    public static MovieDetails dummy() {
        if (DUMMY == null) {
             DUMMY = new MovieDetails();
        }
        return DUMMY;
    }
}
