package test.ksubaka.com.data.store;

import java.util.List;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.entity.MoviesResult;

/**
 * Created by Lucian on 1/18/2017.
 */

public interface MoviesDataStore {
    Observable<MoviesResult> getMovies(String query);

    Observable<MovieDetails> getMovieDetails(String id);
}
