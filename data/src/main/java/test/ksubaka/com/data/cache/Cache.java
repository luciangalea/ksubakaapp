package test.ksubaka.com.data.cache;

import java.util.List;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;

/**
 * Created by Lucian on 1/18/2017.
 */

public interface Cache {
    Observable<List<Movie>> getMovies(String query);

    Observable<MovieDetails> getMovieDetails(String id);

    void storeMovies(List<Movie> movies, String query);

    void storeMovieDetails(String id, MovieDetails movieDetails);
}
