package test.ksubaka.com.data.entity;

import com.google.gson.annotations.SerializedName;

import javax.inject.Singleton;

/**
 * Created by Lucian on 1/18/2017.
 */

public class Movie {
    @SerializedName("imdbID")
    private String imdbID;
    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("Poster")
    private String poster;

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        return imdbID.equals(movie.imdbID);

    }

    @Override
    public int hashCode() {
        return imdbID.hashCode();
    }
}
