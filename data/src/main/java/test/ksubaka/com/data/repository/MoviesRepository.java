package test.ksubaka.com.data.repository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.Observable;
import test.ksubaka.com.data.cache.Cache;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.entity.MoviesResult;
import test.ksubaka.com.data.store.MoviesDataStore;

/**
 * Created by Lucian on 1/18/2017.
 */

@Singleton
public class MoviesRepository {
    private Cache memoryCache;
    private MoviesDataStore moviesDataStore;

    @Inject
    public MoviesRepository(@Named("memory") Cache memoryCache, MoviesDataStore moviesDataStore) {
        this.memoryCache = memoryCache;
        this.moviesDataStore = moviesDataStore;
    }

    public Observable<List<Movie>> getMovies(String query) {
        return Observable.merge(
                memoryCache.getMovies(query).filter(movies -> !movies.isEmpty()),
                moviesDataStore.getMovies(query)
                        .map(MoviesResult::getMovies)
                        .doOnNext(movies -> memoryCache.storeMovies(movies, query))
                        .onErrorReturnItem(new ArrayList<>())
        ).take(1);
    }

    public Observable<MovieDetails> getMovieDetails(String id) {
        return Observable.merge(
                memoryCache.getMovieDetails(id).filter(movie -> movie != MovieDetails.dummy()),
                moviesDataStore.getMovieDetails(id)
                    .doOnNext(movie -> memoryCache.storeMovieDetails(id, movie))
        ).take(1);
    }
}
