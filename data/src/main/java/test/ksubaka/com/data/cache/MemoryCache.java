package test.ksubaka.com.data.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MemoryCache implements Cache {
    private Map<String, List<Movie>> moviesCacheMap = new HashMap<>();
    private Map<String, MovieDetails> movieDetailsCacheMap = new HashMap<>();

    @Override
    public Observable<List<Movie>> getMovies(String query) {
        //RxJava2 doesn't allow emitting nulls, so we emit an empty list instead
        List<Movie> movies = moviesCacheMap.get(query);
        return Observable.just(
                movies != null ? movies : new ArrayList<Movie>()
        );
    }

    @Override
    public Observable<MovieDetails> getMovieDetails(String id) {
        return Observable.just(
                movieDetailsCacheMap.containsKey(id) ? movieDetailsCacheMap.get(id) : MovieDetails.dummy()
        );
    }

    @Override
    public void storeMovies(List<Movie> movies, String query) {
        this.moviesCacheMap.put(query, movies);
    }

    @Override
    public void storeMovieDetails(String id, MovieDetails movieDetails) {
        movieDetailsCacheMap.put(id, movieDetails);
    }
}
