package test.ksubaka.com.data.store;

import java.util.List;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.entity.MoviesResult;

/**
 * Created by Lucian on 1/18/2017.
 */

public class ClouldMoviesDataStore implements MoviesDataStore {
    private OMDbApi api;

    public ClouldMoviesDataStore(OMDbApi api) {
        this.api = api;
    }

    @Override
    public Observable<MoviesResult> getMovies(String query) {
        return api.getMovies(query);
    }

    @Override
    public Observable<MovieDetails> getMovieDetails(String id) {
        return api.getMovieDetails(id);
    }
}
