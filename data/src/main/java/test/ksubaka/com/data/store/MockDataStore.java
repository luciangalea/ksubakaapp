package test.ksubaka.com.data.store;

import java.util.Arrays;

import io.reactivex.Observable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.data.entity.MoviesResult;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MockDataStore implements MoviesDataStore {
    private MoviesResult result;
    private MovieDetails movieDetails;

    public MockDataStore() {
        buildData();
    }

    @Override
    public Observable<MoviesResult> getMovies(String query) {
        return Observable.just(result);
    }

    @Override
    public Observable<MovieDetails> getMovieDetails(String id) {
        return Observable.just(movieDetails);
    }

    private void buildData() {
        Movie movie1 = new Movie();
        movie1.setImdbID("movie1");
        movie1.setPoster("http://vignette2.wikia.nocookie.net/ideas/images/e/e4/Movie_night.jpg/revision/latest?cb=20141222232947");
        movie1.setTitle("Movie 1");
        movie1.setYear("1992");

        Movie movie2 = new Movie();
        movie2.setImdbID("movie2");
        movie2.setPoster("http://vignette2.wikia.nocookie.net/ideas/images/e/e4/Movie_night.jpg/revision/latest?cb=20141222232947");
        movie2.setTitle("Movie 2");
        movie2.setYear("1992");

        result = new MoviesResult();
        result.setMovies(Arrays.asList(movie1, movie2));

        movieDetails = new MovieDetails();
        movieDetails.setTitle("Movie1");
        movieDetails.setImdbID("movie1");
        movieDetails.setActors("Some famous actors");
        movieDetails.setDirector("Some famous director");
        movieDetails.setGenre("Drama");
        movieDetails.setPoster("http://vignette2.wikia.nocookie.net/ideas/images/e/e4/Movie_night.jpg/revision/latest?cb=20141222232947");
    }

    public MoviesResult getResult() {
        return result;
    }

    public MovieDetails getMovieDetails() {
        return movieDetails;
    }
}
