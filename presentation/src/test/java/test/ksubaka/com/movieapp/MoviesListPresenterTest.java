package test.ksubaka.com.movieapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import test.ksubaka.com.domain.interactor.GetMoviesInteractor;
import test.ksubaka.com.movieapp.presenter.MovieListPresenter;
import test.ksubaka.com.movieapp.view.MovieListView;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Lucian on 1/18/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class MoviesListPresenterTest {
    @Mock
    GetMoviesInteractor getMoviesInteractor;
    @Mock
    MovieListView view;

    private MovieListPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new MovieListPresenter(getMoviesInteractor);
        presenter.attachView(view);
    }

    @Test
    public void testRenderError() {
        String query = "test";

        when(getMoviesInteractor.withData(query)).thenReturn(getMoviesInteractor);
        when(getMoviesInteractor.execute()).thenReturn(Observable.error(new Exception("Test")));
        presenter.onSearchQueryEntered(query);
        verify(view).renderMoviesLoadingError();
    }
}
