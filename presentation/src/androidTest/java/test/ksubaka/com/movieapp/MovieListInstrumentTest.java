package test.ksubaka.com.movieapp;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.data.entity.MoviesResult;
import test.ksubaka.com.data.store.MockDataStore;
import test.ksubaka.com.domain.interactor.configuration.Configuration;
import test.ksubaka.com.domain.interactor.configuration.ConfigurationImpl;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by Lucian on 1/18/2017.
 */

@RunWith(AndroidJUnit4.class)
public class MovieListInstrumentTest {
    @Rule
    public IntentsTestRule<MovieListActivity> rule = new IntentsTestRule<>(MovieListActivity.class);

    private MoviesResult result = new MockDataStore().getResult();
    private Configuration configuration = new ConfigurationImpl();
    private IdlingResource idlingResource;

    @After
    public void tearDown() {
        if (idlingResource != null) {
            Espresso.unregisterIdlingResources(idlingResource);
        }
    }

    @Test
    public void testMovieRendering() {
        onView(withId(R.id.action_search)).perform(click());
        onView(isAssignableFrom(EditText.class)).perform(typeText("Test"));

        idlingResource = new WaitIdlingResource(configuration.searchDebounce());
        Espresso.registerIdlingResources(idlingResource);

        onView(withText(result.getMovies().get(0).getTitle())).check(matches(isDisplayed()));
    }

    @Test
    public void testMovieSelected() {
        Movie movie = result.getMovies().get(0);

        onView(withId(R.id.action_search)).perform(click());
        onView(isAssignableFrom(EditText.class)).perform(typeText("Test"));

        idlingResource = new WaitIdlingResource(configuration.searchDebounce());
        Espresso.registerIdlingResources(idlingResource);

        onView(withText(movie.getTitle())).perform(click());
        intended(allOf(
                hasComponent(MovieDetailsActivity.class.getName()),
                hasExtra(MovieDetailsActivity.ARG_MOVIE_ID, movie.getImdbID()),
                hasExtra(MovieDetailsActivity.ARG_MOVIE_TITLE, movie.getTitle())
        ));
    }


    //we need this for the debounce in the search
    private class WaitIdlingResource implements IdlingResource {
        private long start;
        private long duration;
        private ResourceCallback callback;

        private WaitIdlingResource(long duration) {
            this.duration = duration;
            this.start = System.currentTimeMillis();
        }

        @Override
        public String getName() {
            return WaitIdlingResource.class.getName();
        }

        @Override
        public boolean isIdleNow() {
            boolean idle = System.currentTimeMillis() - start > duration;
            if (idle && callback != null) {
                callback.onTransitionToIdle();
            }
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback callback) {
            this.callback = callback;
        }
    }
}
