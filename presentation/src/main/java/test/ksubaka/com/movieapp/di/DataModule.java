package test.ksubaka.com.movieapp.di;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.ksubaka.com.data.cache.Cache;
import test.ksubaka.com.data.cache.MemoryCache;
import test.ksubaka.com.data.store.ClouldMoviesDataStore;
import test.ksubaka.com.data.store.MoviesDataStore;
import test.ksubaka.com.data.store.OMDbApi;
import test.ksubaka.com.movieapp.R;

/**
 * Created by Lucian on 1/11/2017.
 */

@Module
public class DataModule {
    @Provides
    @Singleton
    MoviesDataStore providesMoviesDataStore(Context context) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(context.getString(R.string.base_url))
                .build();
        return new ClouldMoviesDataStore(retrofit.create(OMDbApi.class));
    }

    @Provides
    @Named("memory")
    Cache providesMemoryCache() {
        return new MemoryCache();
    }
}
