package test.ksubaka.com.movieapp;

import android.app.Application;

import test.ksubaka.com.movieapp.di.ApplicationComponent;
import test.ksubaka.com.movieapp.di.ApplicationModule;
import test.ksubaka.com.movieapp.di.DaggerApplicationComponent;
import test.ksubaka.com.movieapp.di.DataModule;

/**
 * Created by Lucian on 1/18/2017.
 */

public class KsubakaApp extends Application {
    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null) {
            applicationComponent = createApplicationComponent();
        }
        return applicationComponent;
    }

    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule())
                .build();
    }
}
