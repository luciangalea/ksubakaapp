package test.ksubaka.com.movieapp.di;

import test.ksubaka.com.movieapp.KsubakaApp;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MockApplication extends KsubakaApp {
    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new MockDataModule())
                .build();
    }
}
