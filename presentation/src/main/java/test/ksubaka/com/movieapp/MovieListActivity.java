package test.ksubaka.com.movieapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.domain.interactor.configuration.Configuration;
import test.ksubaka.com.movieapp.adapter.MovieAdapter;
import test.ksubaka.com.movieapp.presenter.MovieListPresenter;
import test.ksubaka.com.movieapp.view.MovieListView;

public class MovieListActivity extends AppCompatActivity implements MovieListView {
    public static final String KEY_SEARCH_QUERY = "keySearchQuery";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_movies)
    RecyclerView moviesRecyclerView;
    @BindView(R.id.txt_no_movies)
    TextView emptyListTextView;
    @BindView(R.id.container)
    ViewGroup container;

    @Inject
    MovieListPresenter presenter;
    @Inject
    Configuration configuration;

    private MovieAdapter moviesAdapter;
    private SearchView searchView;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ((KsubakaApp) getApplication()).getApplicationComponent().inject(this);
        moviesAdapter = new MovieAdapter(presenter::onMovieSelected);
        moviesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        moviesRecyclerView.setAdapter(moviesAdapter);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_SEARCH_QUERY)) {
            query = savedInstanceState.getString(KEY_SEARCH_QUERY);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movies_list, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        initQueryListener();

        if (!TextUtils.isEmpty(query)) {
            searchItem.expandActionView();
            searchView.setQuery(query, true);
        }
        return true;
    }

    private void initQueryListener() {
        Observable.<String>create(e ->
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        e.onNext(query);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        e.onNext(newText);
                        return true;
                    }
                }))
                .debounce(configuration.searchDebounce(), TimeUnit.MILLISECONDS)
                .filter(s -> s.length() >= configuration.minNumberOfCharactersForSearch())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(presenter::onSearchQueryEntered);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (searchView != null) {
            String query = searchView.getQuery().toString();
            if (!TextUtils.isEmpty(query))
                outState.putString(KEY_SEARCH_QUERY, query);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void renderEmptyMovieList() {
        moviesRecyclerView.setVisibility(View.GONE);
        emptyListTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderMovies(List<Movie> movies) {
        moviesRecyclerView.setVisibility(View.VISIBLE);
        emptyListTextView.setVisibility(View.GONE);

        moviesAdapter.updateMovies(movies);
        moviesAdapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToMovieDetailsScreen(String imdbID, String title) {
        startActivity(new Intent(this, MovieDetailsActivity.class)
                .putExtra(MovieDetailsActivity.ARG_MOVIE_ID, imdbID)
                .putExtra(MovieDetailsActivity.ARG_MOVIE_TITLE, title));
    }

    @Override
    public void renderMoviesLoadingError() {
        Snackbar.make(container, R.string.error_loading_movies, Snackbar.LENGTH_LONG).show();
    }
}
