package test.ksubaka.com.movieapp.di;

import javax.inject.Singleton;

import dagger.Component;
import test.ksubaka.com.movieapp.MovieDetailsActivity;
import test.ksubaka.com.movieapp.MovieListActivity;

/**
 * Created by Lucian on 1/11/2017.
 */

@Component(modules = {ApplicationModule.class, DataModule.class})
@Singleton
public interface ApplicationComponent {
    void inject(MovieListActivity activity);

    void inject(MovieDetailsActivity activity);
}
