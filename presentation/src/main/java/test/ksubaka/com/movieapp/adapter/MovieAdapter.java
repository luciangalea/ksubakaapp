package test.ksubaka.com.movieapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.movieapp.R;
import test.ksubaka.com.movieapp.databinding.CellMovieBinding;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MovieAdapter  extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private OnMovieSelectedListener onMovieSelectedListener;
    private List<Movie> movies = new ArrayList<>();

    public MovieAdapter(OnMovieSelectedListener onMovieSelectedListener) {
        this.onMovieSelectedListener = onMovieSelectedListener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieViewHolder(CellMovieBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false
        ));
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(movies.get(position));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void updateMovies(List<Movie> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        private final CellMovieBinding movieBinding;

        MovieViewHolder(CellMovieBinding movieBinding) {
            super(movieBinding.getRoot());
            this.movieBinding = movieBinding;
        }

        void bind(Movie movie) {
            movieBinding.setMovie(movie);
            movieBinding.setClickListener(onMovieSelectedListener);
            movieBinding.executePendingBindings();
        }
    }

    public interface OnMovieSelectedListener {
        void onMovieSelected(Movie movie);
    }
}
