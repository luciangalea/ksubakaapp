package test.ksubaka.com.movieapp.presenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import test.ksubaka.com.data.entity.Movie;
import test.ksubaka.com.domain.interactor.GetMoviesInteractor;
import test.ksubaka.com.movieapp.view.MovieListView;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MovieListPresenter extends BasePresenter<MovieListView> {
    private GetMoviesInteractor getMoviesInteractor;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    public MovieListPresenter(GetMoviesInteractor getMoviesInteractor) {
        this.getMoviesInteractor = getMoviesInteractor;
    }

    public void onSearchQueryEntered(String query) {
        disposable.add(getMoviesInteractor.withData(query).execute().subscribe(movies -> {
                    if (movies.isEmpty()) {
                        view.renderEmptyMovieList();
                    } else {
                        view.renderMovies(movies);
                    }
                }, error -> view.renderMoviesLoadingError()
        ));
    }

    public void onMovieSelected(Movie movie) {
        view.navigateToMovieDetailsScreen(movie.getImdbID(), movie.getTitle());
    }

    @Override
    protected void cleanup() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.clear();
        }
    }
}
