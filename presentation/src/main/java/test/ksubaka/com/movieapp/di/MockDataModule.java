package test.ksubaka.com.movieapp.di;

import android.content.Context;

import test.ksubaka.com.data.store.MockDataStore;
import test.ksubaka.com.data.store.MoviesDataStore;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MockDataModule extends DataModule {
    @Override
    MoviesDataStore providesMoviesDataStore(Context context) {
        return new MockDataStore();
    }
}
