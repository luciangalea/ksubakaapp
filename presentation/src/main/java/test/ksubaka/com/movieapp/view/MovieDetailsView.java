package test.ksubaka.com.movieapp.view;

import test.ksubaka.com.data.entity.MovieDetails;

/**
 * Created by Lucian on 1/18/2017.
 */

public interface MovieDetailsView extends View {
    void renderMovieDetails(MovieDetails movieDetails);

    void renderMovieDetailsLoadingError();
}
