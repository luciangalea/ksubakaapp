package test.ksubaka.com.movieapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.ksubaka.com.data.entity.MovieDetails;
import test.ksubaka.com.movieapp.databinding.ActivityMovieDetailsBinding;
import test.ksubaka.com.movieapp.presenter.MovieDetailsPresenter;
import test.ksubaka.com.movieapp.view.MovieDetailsView;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MovieDetailsActivity extends AppCompatActivity implements MovieDetailsView {
    public static final String ARG_MOVIE_ID = "argMovieId";
    public static final String ARG_MOVIE_TITLE = "argMovieTitle";

    @Inject
    MovieDetailsPresenter presenter;
    @BindView(R.id.container)
    ViewGroup container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ActivityMovieDetailsBinding binding;
    private String movieId;
    private String movieTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        readExtras();
        setTitle(movieTitle);
        ((KsubakaApp) getApplication()).getApplicationComponent().inject(this);
    }

    private void readExtras() {
        movieId = getIntent().getStringExtra(ARG_MOVIE_ID);
        movieTitle = getIntent().getStringExtra(ARG_MOVIE_TITLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        presenter.initialize(movieId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public void renderMovieDetails(MovieDetails movieDetails) {
        binding.setMovie(movieDetails);
    }

    @Override
    public void renderMovieDetailsLoadingError() {
        Snackbar.make(container, R.string.error_loading_movie_details, Snackbar.LENGTH_LONG).show();
    }
}
