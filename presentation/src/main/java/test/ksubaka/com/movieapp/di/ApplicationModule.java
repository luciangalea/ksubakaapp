package test.ksubaka.com.movieapp.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import test.ksubaka.com.domain.interactor.configuration.Configuration;
import test.ksubaka.com.domain.interactor.configuration.ConfigurationImpl;
import test.ksubaka.com.domain.interactor.thread.InteractorThread;


/**
 * Created by Lucian on 1/11/2017.
 */

@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    Context providesContext() {
        return application;
    }

    @Provides
    @Named("observeThread")
    InteractorThread providesObserveThread() {
        return AndroidSchedulers::mainThread;
    }

    @Provides
    @Named("subscribeThread")
    InteractorThread providesSubscribeTread() {
        return Schedulers::io;
    }

    @Provides
    @Singleton
    Configuration providesConfiguration() {
        return new ConfigurationImpl();
    }
}
