package test.ksubaka.com.movieapp.presenter;


import test.ksubaka.com.movieapp.view.View;

/**
 * Created by Lucian on 1/12/2017.
 */

public class BasePresenter<T extends View> {
    protected T view;

    public void attachView(T view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        cleanup();
    }

    protected void cleanup() {

    }
}
