package test.ksubaka.com.movieapp.view;

import java.util.List;

import test.ksubaka.com.data.entity.Movie;

/**
 * Created by Lucian on 1/18/2017.
 */

public interface MovieListView extends View {
    void renderEmptyMovieList();

    void renderMovies(List<Movie> movies);

    void renderMoviesLoadingError();

    void navigateToMovieDetailsScreen(String imdbID, String title);
}
