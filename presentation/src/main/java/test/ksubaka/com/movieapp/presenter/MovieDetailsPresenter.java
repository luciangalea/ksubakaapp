package test.ksubaka.com.movieapp.presenter;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import test.ksubaka.com.domain.interactor.GetMovieDetailsInteractor;
import test.ksubaka.com.domain.interactor.GetMoviesInteractor;
import test.ksubaka.com.movieapp.view.MovieDetailsView;

/**
 * Created by Lucian on 1/18/2017.
 */

public class MovieDetailsPresenter extends BasePresenter<MovieDetailsView> {
    private GetMovieDetailsInteractor movieDetailsInteractor;
    private Disposable disposable;

    @Inject
    public MovieDetailsPresenter(GetMovieDetailsInteractor movieDetailsInteractor) {
        this.movieDetailsInteractor = movieDetailsInteractor;
    }

    public void initialize(String id) {
        disposable = movieDetailsInteractor.withData(id).execute().subscribe(view::renderMovieDetails,
                error -> view.renderMovieDetailsLoadingError());
    }

    @Override
    protected void cleanup() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
